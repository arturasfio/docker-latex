FROM aergus/latex

RUN apt-get update -qq \
    && apt-get install  -qq -y  \
	biber \
	fonts-texgyre \
	inkscape \ 
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
	