# docker-latex

This Docker image is based on [`aergus/latex`](https://hub.docker.com/r/aergus/latex/) image. The image was primarily created for Inkscape support, which is useful for `.svg` file conversion when compiling LaTeX files.
